Source: r-cran-bayesplot
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-dplyr (>= 0.8.0),
               r-cran-ggplot2,
               r-cran-ggridges,
               r-cran-glue,
               r-cran-posterior,
               r-cran-reshape2,
               r-cran-rlang,
               r-cran-tibble,
               r-cran-tidyselect
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-bayesplot
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-bayesplot.git
Homepage: https://cran.r-project.org/package=bayesplot
Rules-Requires-Root: no

Package: r-cran-bayesplot
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R plotting for bayesian models
 Plotting functions for posterior analysis, model checking,
 and MCMC diagnostics. The package is designed not only to provide convenient
 functionality for users, but also a common set of functions that can be
 easily used by developers working on a variety of R packages for Bayesian
 modeling, particularly (but not exclusively) packages interfacing with 'Stan'.
